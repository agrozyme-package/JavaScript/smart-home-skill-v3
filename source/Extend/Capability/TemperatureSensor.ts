import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import {TemperatureSensorReponse} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/TemperatureSensor';
import Capability from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class TemperatureSensor extends Capability {
  async run(): Promise<TemperatureSensorReponse> {
    const properties = await this.dispatch();
    const endpoint = this.skill.getEndpoint();
    const header = this.makeHeader(ResponseName.StateReport);
    return {
      context: {properties},
      event: {
        header,
        endpoint,
        payload: {}
      }
    };
  }

}
