import {BrightnessControllerRequestName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/BrightnessController';
import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import {BrightnessControllerReponse} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/BrightnessController';
import Property from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Property';
import Capability, {Actions} from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class BrightnessController extends Capability {
  async run(): Promise<BrightnessControllerReponse> {
    const properties = await this.dispatch();
    const endpoint = this.skill.getEndpoint();
    const header = this.makeHeader(ResponseName.StateReport);
    return {
      context: {properties},
      event: {
        header,
        endpoint,
        payload: {}
      }
    };
  }

  protected async doAdjustBrightness(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected async doSetBrightness(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected makeActions(): Actions {
    const items = super.makeActions();
    items[BrightnessControllerRequestName.AdjustBrightness] = this.doAdjustBrightness;
    items[BrightnessControllerRequestName.SetBrightness] = this.doSetBrightness;
    return items;
  }

}

