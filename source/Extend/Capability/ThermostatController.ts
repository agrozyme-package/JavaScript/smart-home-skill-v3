import {ThermostatControllerRequestName} from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Capability/ThermostatController';
import ResponseName from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/ResponseName';
import {ThermostatControllerReponse} from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Capability/ThermostatController';
import Property from '@agrozyme/alexa-skill-type/source/SmartHome/Type/Property';
import Capability, {Actions} from '../Base/Capability';

// noinspection JSUnusedGlobalSymbols
export default abstract class ThermostatController extends Capability {
  async run(): Promise<ThermostatControllerReponse> {
    const properties = await this.dispatch();
    const endpoint = this.skill.getEndpoint();
    const header = this.makeHeader(ResponseName.StateReport);
    return {
      context: {properties},
      event: {
        header,
        endpoint,
        payload: {}
      }
    };
  }

  protected async doAdjustTargetTemperature(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected async doSetTargetTemperature(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected async doSetThermostatMode(): Promise<Property[]> {
    throw this.skill.error.alexa.getInternalError('Not Support');
  }

  protected makeActions(): Actions {
    const items = super.makeActions();
    items[ThermostatControllerRequestName.SetTargetTemperature] = this.doSetTargetTemperature;
    items[ThermostatControllerRequestName.AdjustTargetTemperature] = this.doAdjustTargetTemperature;
    items[ThermostatControllerRequestName.SetThermostatMode] = this.doSetThermostatMode;
    return items;
  }

}
