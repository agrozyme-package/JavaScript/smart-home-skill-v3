import VideoError from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/Error/Video';
import SkillNamespace from '@agrozyme/alexa-skill-type/source/SmartHome/Enumeration/SkillNamespace';
import SkillRequest from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import Base from '../Extend/Base/Error';

export default class Video extends Base {

  constructor(event: SkillRequest) {
    super(event);
    this.header.namespace = SkillNamespace.Video;
  }

  getCookDurationTooLong(message: string) {
    const payload = {
      type: VideoError.NOT_SUBSCRIBED,
      message
    };
    return this.makeError(payload);
  }

}
