import SkillRequest from '@agrozyme/alexa-skill-type/source/SmartHome/Type/SkillRequest';
import Alexa from './Alexa';
import Cooking from './Cooking';
import Video from './Video';

export default class AlexaError {
  readonly alexa: Alexa;
  readonly cooking: Cooking;
  readonly video: Video;

  constructor(event: SkillRequest) {
    this.alexa = new Alexa(event);
    this.cooking = new Cooking(event);
    this.video = new Video(event);
  }

}
